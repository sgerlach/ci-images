FROM registry.suse.com/bci/bci-base:15.5

LABEL Description="KDE Suse Linux Enterprise Server 15 Base for use with Craft"
LABEL maintainer="KDE Sysadmin <sysadmin@kde.org>"

RUN zypper --non-interactive update && \
    zypper --non-interactive install openssh-server \
    # install some core tools
    which git-core cmake gcc12 gcc12-c++ glibc-devel-static xz \
    # fortran needed to compile r-base
    gcc12-fortran \
    # python
    python311 python311-pip python311-devel sqlite3 zlib \
    # localedef
    glibc-i18ndata \
    # qt
    vulkan-devel vulkan-headers wayland-devel Mesa-libGL-devel \
    libX11-devel libX11-xcb1 libxcb-devel xcb-util-keysyms-devel xcb-util-renderutil-devel xcb-util-wm-devel libxkbcommon-x11-devel libxkbcommon-devel libXi-devel xcb-util-image-devel \
    flex bison gperf libicu-devel ruby awk patch \
    alsa-devel libXcomposite-devel libXcursor-devel libXrandr-devel libXtst-devel mozilla-nspr-devel mozilla-nss-devel gperf bison nodejs-default nodejs-devel-default \
    # QtWebEngine
    libxkbfile-devel libdrm-devel libXdamage-devel libxshmfence-devel \
    # KDE Frameworks: Solid
    libudev-devel \
    # KDE Frameworks: KIO
    libmount-devel \
    # KDE Frameworks: KWayland
    Mesa-libEGL-devel \
    # python2
    zlib-devel sqlite3-devel libopenssl-1_1-devel && \
    # cleanup to ensure we don't leave behind anything that doesn't need to be in the image
    zypper --non-interactive clean -a && \
    # make sure the appropriate gcc/python versions are the defaults
    alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-12 1 && \
    alternatives --install /usr/bin/cc cc /usr/bin/gcc-12 1 && \
    alternatives --install /usr/bin/cpp cpp /usr/bin/g++-12 1 && \
    alternatives --install /usr/bin/g++ gcc++ /usr/bin/g++-12 1 && \
    alternatives --install /usr/bin/gfortran gfortran /usr/bin/gfortran-12 1 && \
    alternatives --install /usr/bin/python python /usr/bin/python3.11 1 && \
    alternatives --install /usr/bin/python3 python3 /usr/bin/python3.11 1 && \
    alternatives --install /usr/bin/pip3 pip3 /usr/bin/pip3-3.11 1

# install powershell
RUN if [ $(arch) == "x86_64" ]; then \
        pwshArch="x64"; \
    else \
        pwshArch="arm64"; \
    fi && \
    curl -L -o /tmp/powershell.tar.gz https://github.com/PowerShell/PowerShell/releases/download/v7.4.0/powershell-7.4.0-linux-$pwshArch.tar.gz && \
    mkdir -p /opt/microsoft/powershell/7 && \
    tar zxf /tmp/powershell.tar.gz -C /opt/microsoft/powershell/7 && \
    rm /tmp/powershell.tar.gz && \
    chmod +x /opt/microsoft/powershell/7/pwsh && \
    ln -s /opt/microsoft/powershell/7/pwsh /usr/bin/pwsh

# install python modules needed for CI/CD tooling (upload, signing service,...)
RUN python3 --version && \
     python3 -m pip install --upgrade pip && \
     python3 -m pip install pyyaml lxml paramiko requests

# Setup a user account for everything else to be done under
RUN useradd -d /home/appimage/ -u 1000 --user-group --create-home -G video appimage

# Get locales in order
RUN localedef -c -i en_US -f UTF-8 en_US.UTF-8

# let git work
RUN git config --system merge.defaultToUpstream true

# required by qtbase and not available in the repository
RUN curl -OL https://xcb.freedesktop.org/dist/xcb-util-cursor-0.1.4.tar.xz && \
    tar xf xcb-util-cursor-0.1.4.tar.xz && \
    cd xcb-util-cursor-0.1.4 && \
    ./configure --prefix /usr/ --enable-static --disable-shared CFLAGS=-fPIC && \
    make -j5 && \
    make install && \
    cd .. && \
    rm -Rf xcb-util-cursor-0.1.4

# xorg-util-macros is a dependency of glproto (see below)
RUN curl -OL https://www.x.org/releases/individual/util/util-macros-1.20.0.tar.xz && \
    tar xf util-macros-1.20.0.tar.xz && \
    cd util-macros-1.20.0 && \
    ./configure && \
    make -j5 && \
    make install && \
    cd .. && \
    rm -Rf util-macros-1.20.0

# glproto is required by qtwebengine and not available in the repository
RUN curl -OL https://www.x.org/archive/individual/proto/glproto-1.4.17.tar.gz && \
    tar xf glproto-1.4.17.tar.gz && \
    cd glproto-1.4.17 && \
    ./configure && \
    make -j5 && \
    make install && \
    cd .. && \
    rm -Rf glproto-1.4.17.tar.gz

# Python 2 For Qt5 WebEngine :( 
ENV PYENV_ROOT=/opt/pyenv
ENV PATH="${PATH}:${PYENV_ROOT}/shims"
RUN git clone https://github.com/pyenv/pyenv.git "$PYENV_ROOT" && \
    command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH" && \
    eval "$(pyenv init -)" && \
    pyenv install 2.7.18 && \
    # this enables the pyenv shims. as we appened the path we will only be affected by the python2 shim
    pyenv global system 2.7.18

# Finally drop to an unprivileged user account
USER appimage
